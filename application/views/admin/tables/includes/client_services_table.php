<?php
if ($invoice_items <> null && count($invoice_items) > 0) {
    foreach ($invoice_items as $items) {
        ?>
        <tr class="client-items-<?= $items->userid; ?> items-renewals">
            <td>
                <a href="/admin/invoices/list_invoices/<?= $items->rel_id; ?>">
                    <?= format_invoice_number($items->rel_id); ?>
                    <input type="hidden" id="inv-<?= $items->userid; ?>"
                           value="<?= $items->rel_id; ?>"/>
                </a>
            </td>
            <td>
                <b><?= $items->item_name . '</b><br/>' . getDescriptionFristLine($items->item_description) . '<br/>' . $items->qty . ' * ' . $items->item_rate; ?>
            </td>
            <td><?= $items->qty * $items->item_rate; ?></td>
            <td><?= date("d-m-Y", strtotime($items->item_start_date)); ?></td>
            <td><?= date("d-m-Y", strtotime($items->item_end_date)); ?></td>
            <td>
                <?php
                $invoice_accepted = get_invoice_status(trim($items->rel_id));
                $invStatus = '<span id="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                if ($invoice_accepted <> null) {

                    if ($invoice_accepted->invst_status == 'open') {
                        $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                    }
                    if ($invoice_accepted->invst_status == 'accepted') {
                        $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label-success label">ACCEPTED</span></span>';
                    }
                    if ($invoice_accepted->invst_status == 'rejected') {
                        $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-danger">REJECTED</span></span>';
                    }
                }

                echo $invStatus;
                ?>
            </td>
        </tr>
        <?php
    }
} else {
    ?>
    <tr class="client-items-<?= $items->userid; ?> items-renewals">
        <td colspan="6" style="text-align: center;"><span class="label label-warning">No record found</span></td>
    </tr>
<?php
}?>
