<div class="row">

    <div class="panel_s">
        <div class="panel-body">
            <h4 class="no-mtop">
                <?= _l('receipt_details'); ?>
            </h4>
            <hr class="hr-panel-heading">
            <table cellspacing="0" class="table table-striped table-responsive dt-responsive"
                   id="ReceiptTable">
                <thead>
                <tr role="row">
                    <th><?= _l('receipt_number'); ?></th>
                    <th><?= _l('receipt_date'); ?></th>
                    <th><?= _l('slip_number'); ?></th>
                    <th><?= _l('receipt_amount'); ?></th>
                    <th><?= _l('receipt_type'); ?></th>
                    <th><?= _l('receipt_cheque_date'); ?></th>
                    <th><?= _l('receipt_note'); ?></th>
                    <th><?= _l('receipt_status'); ?></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody id="invoices_data">
                <?php
                if ($receipts != null && count($receipts) > 0) {
                    $i = 0;
                    foreach ($receipts as $receipt) {
                        $created_by = '';
                        $rec_id = $receipt->receipt_id;
                        if ($receipt->reciept_owner <> null) {
                            $reciept_owner = $this->receipts_model->staffNameById($receipt->reciept_owner);
                        }
                        if ($receipt->receipt_created_by <> null) {
                            $created_by = $this->receipts_model->staffNameById($receipt->receipt_created_by);
                        }
                        ?>
                        <tr>
                            <td>
                                <a href="<?= base_url(); ?>admin/receipts/details/<?= $receipt->receipt_id; ?>"
                                   target="_blank"> <?= $receipt->receipt_num; ?>  </a>
                            </td>
                            <td><?= date('d-m-Y', strtotime($receipt->receipt_date)); ?></td>
                            <td><?= $receipt->receipt_slip_no; ?>  </td>
                            <td><?= $receipt->receipt_amount; ?>  </td>
                            <td><?= $receipt->receipt_type; ?>  </td>
                            <td><?= date('d-m-Y', strtotime($receipt->receipt_cheque_date)); ?></td>
                            <td><?= $receipt->receipt_note; ?>  </td>
                            <td>
                                <div class="form-group">
                                    <?php if (is_admin() || is_staff_member()) {
                                        $status_rec = $receipt->receipt_status;
                                        ?>
                                        <select id="<?= $rec_id; ?>" name="data[status]"
                                                class="btn btn-mini form-control change_status">
                                            <option value="created" <?= ($status_rec == 'created') ? 'selected' : '' ?>><?= _l('receipt_created'); ?></option>
                                            <?php if ($canHandover) {
                                                ?>
                                                <option value="handover" <?= ($status_rec == 'handover') ? 'selected' : '' ?> ><?= _l('receipt_handover'); ?></option>
                                                <?php
                                            } ?>

                                            <?php if ($canDesposit) {
                                                ?>
                                                <option value="deposited" <?= ($status_rec == 'deposited') ? 'selected' : '' ?> ><?= _l('receipt_deposited'); ?></option>
                                                <?php
                                            } ?>
                                            <?php if ($canVerify) {
                                                ?>
                                                <option value="verified" <?= ($status_rec == 'verified') ? 'selected' : '' ?> ><?= _l('receipt_verified'); ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    <?php } else { ?>
                                        <span class="label label-success  s-status"> <?= (isset($status_rec) && $status_rec) ? ucfirst($status_rec) : 'Created'; ?></span>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button class="label label-default-light dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?= base_url(); ?>admin/receipts/details/<?= $rec_id; ?>"
                                               id="<?= $rec_id; ?>"
                                               class="text text-primary" target="_blank">Preview</a>
                                        </li>
                                        <li>
                                            <a href="<?= base_url(); ?>admin/receipts/update/<?= $rec_id; ?>"
                                               id="<?= $rec_id; ?>"
                                               class="text text-primary" target="_blank">Edit</a>
                                        </li>
                                        <?php
                                        if (is_admin()) {
                                            ?>
                                            <li>
                                                <a href="#"
                                                   id="<?= $rec_id; ?>"
                                                   class="delete text text-danger">Delete</a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script>

    $(document).ready(function () {
        var table = $('#ReceiptTable').DataTable({
            "order": [[0, "desc"]]
        });
    });

    $(document).ready(function () {
        $("a.delete").click(function (e) {
            if (!confirm('Are you sure?')) {
                e.preventDefault();
                return false;
            } else {
                var id = $(this).attr('id');
                $.ajax({
                    url: '<?= base_url(); ?>admin/receipts/delete',
                    type: 'POST',
                    data: {'receipt_id': id},
                    success: function (data) {
                        // called when successfull
                        console.log(data);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });

        $('.change_status').change(function () {
            $.ajax({
                url: '<?= base_url(); ?>admin/receipts/updateStatus',
                type: 'POST',
                data: {"changeStatus": true, "status": $(this).val(), "id": $(this).attr('id')},
                success: function (data) {
                    console.log(data);
                    if (data) {
                        alert("Status Updated!");
                    }
                },
                error: function (e) {
                    alert("Error! Status not Updated!");
                }
            });
        });
    });
</script>