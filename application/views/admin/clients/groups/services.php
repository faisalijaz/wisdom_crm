<?php if (isset($client)) {
    ?>
    <h4 class="customer-profile-group-heading"><?php echo _l('Services'); ?></h4>
    <hr class="hr-panel-heading"/>
    <?php echo render_select('group_id', $items_groups, array('id', 'name'), 'customer_services_groups'); ?>
    <input type="hidden" id="group_client_id" value="<?= $client->userid; ?>">
    <table class="table table-striped display" id="InvoiceItems">
        <thead>
        <tr role="row">
            <th>Invoice #</th>
            <th>Item</th>
            <th>Amount</th>
            <th>From Date</th>
            <th>To Date</th>
            <th>Invoice Approval</th>
            <th>Details</th>
        </tr>
        </thead>
        <tbody id="invoice_items">
        <?php
        if ($invoice_items <> null) {
            foreach ($invoice_items as $items) {
                ?>
                <tr class="client-items-<?= $items->userid; ?> items-renewals">
                    <td>
                        <a href="/admin/invoices/list_invoices/<?= $items->rel_id; ?>">
                            <?= format_invoice_number($items->rel_id); ?>
                            <input type="hidden" id="inv-<?= $items->userid; ?>"
                                   value="<?= $items->rel_id; ?>"/>
                        </a>
                    </td>
                    <td>
                        <b><?= $items->item_name . '</b><br/>' . getDescriptionFristLine($items->item_description) . '<br/>' . $items->qty . ' * ' . $items->item_rate; ?>
                    </td>
                    <td><?= $items->qty * $items->item_rate; ?></td>
                    <td><?= date("d-m-Y", strtotime($items->item_start_date)); ?></td>
                    <td><?= date("d-m-Y", strtotime($items->item_end_date)); ?></td>
                    <td>
                        <?php
                        $invoice_accepted = get_invoice_status(trim($items->rel_id));
                        $invStatus = '<span id="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                        if ($invoice_accepted <> null) {

                            if ($invoice_accepted->invst_status == 'open') {
                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                            }
                            if ($invoice_accepted->invst_status == 'accepted') {
                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label-success label">ACCEPTED</span></span>';
                            }
                            if ($invoice_accepted->invst_status == 'rejected') {
                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-danger">REJECTED</span></span>';
                            }
                        }
                        echo $invStatus;
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($items->custom_note != '') {
                            echo '<a href="#" class="renewal_details" id="' . $items->invoice_itmes_id . '">Show</a>';
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="9" style="text-align: right;"></th>
        </tr>
        </tfoot>
    </table>
<?php } ?>

<div id="myModal" class="modal fade col-lg-12" role="dialog" aria-hidden="true"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body" id="content_renewal">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<a href="#" id="modalBox" data-toggle="modal" data-target="#myModal" class="hidden"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        // getGroupsItems();

        $(".renewal_details").click(function (e) {
            e.preventDefault();

            var item_id = $(this).attr('id');
            $.ajax({
                url: "/admin/invoices/get_renewal_item_detail",
                type: 'POST',
                data: {"itemId": item_id},
                success: function (data) {
                    console.log(data);
                    if (data != null && data != "") {
                        // tinymce.get('custom_note').setContent(data);
                        $("#content_renewal").html(data);
                        $("#modalBox").trigger('click');
                    }
                },
                error: function (e) {
                    alert("Error!");
                }
            });
        });
    });
</script>
