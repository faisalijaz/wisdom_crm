<?php
init_head();
$this->load->helper('form');
?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <p class="bold"><?php echo _l('Manage Renewals'); ?></p>
            </div>
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body _buttons">
                        <div class="col-md-12">
                            <div class="row text-center">
                                <div class="col-md-12 text-center" style="    padding: 20px;">
                                    <a href="#" class="btn btn-info " id="create_invoice">Create Invoice</a>
                                </div>
                            </div>
                            <form method="post" enctype="application/x-www-form-urlencoded" action="">
                                <div class="row">
                                    <hr/>
                                    <?php if (has_permission('renewals', '', 'view') || is_admin()) { ?>
                                        <div class="col-md-3">
                                            <?php echo render_select('assigned', $staff, array('staffid', array('firstname', 'lastname')), 'Owner', $this->session->userdata['staff_user_id'], array('data-width' => '100%', 'data-none-selected-text' => 'All')); ?>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-3">
                                        <p class="bold"><?php echo _l('Status'); ?></p>
                                        <select name="status" title="<?php echo _l('additional_filters'); ?>"
                                                id="custom_view" class="selectpicker" data-width="100%">
                                            <option value="expiring" selected>Expiring</option>
                                            <option value="expired">Expired</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <p class="bold"><?php echo _l('in'); ?></p>
                                        <input type="text" name="in" class="form-control" value="1"/>
                                    </div>
                                    <div class="col-md-2">
                                        <p class="bold"><?php echo _l('Duration'); ?></p>
                                        <select name="duration" title="<?php echo _l('additional_filters'); ?>"
                                                id="custom_view" class="selectpicker" data-width="100%">
                                            <option value="day" selected>Day</option>
                                            <option value="months">Month</option>
                                            <option value="year">Year</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3" style="    padding: 20px;">
                                        <p class="bold"><?php echo _l(' '); ?></p>
                                        <input type="submit" class="btn btn-info only-save customer-form-submiter"
                                               value="Filter"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="small-table">
                        <div class="panel_s">
                            <div class="panel-body">
                                <!-- if invoiceid found in url -->
                                <table class="table table-striped display" id="InvoiceItems">
                                    <thead>
                                    <tr role="row">
                                        <th>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary no-mtop checkbox-inline">
                                                    <input type="checkbox" id="" class="" name="isRenewable" value="0"
                                                           disabled>
                                                    <label for="isRenewable-0">&nbsp;</label>
                                                </div>
                                            </div>
                                        </th>
                                        <th>Invoice #</th>
                                        <th>Client</th>
                                        <th>Sales Person</th>
                                        <th>Item</th>
                                        <th>Amount</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Invoice Approval</th>
                                        <th>Action</th>
                                        <td>Details</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    if ($renewal_items <> null) {

                                        foreach ($renewal_items as $items) {

                                            if ($items->userid && format_invoice_number($items->rel_id)) {
                                                ?>
                                                <tr class="client-items-<?= $items->userid; ?> items-renewals">
                                                    <div id=""></div>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="checkbox checkbox-primary no-mtop checkbox-inline">
                                                                <input type="checkbox" id="<?= $items->userid; ?>"
                                                                       class="ClientsItemsRenewable same-client-<?= $items->userid; ?>"
                                                                       name="ItemsRenewable"
                                                                       value="<?= $items->invoice_itmes_id . '-' . $items->userid; ?>">
                                                                <label for="isRenewable-0">&nbsp;</label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a target="_blank" href="/admin/invoices/list_invoices/<?= $items->rel_id; ?>">
                                                            <?= format_invoice_number($items->rel_id); ?>
                                                            <input type="hidden" id="inv-<?= $items->userid; ?>"
                                                                   value="<?= $items->rel_id; ?>"/>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="/admin/clients/client/<?= $items->userid; ?>"><?= $items->company; ?></a>
                                                        <br/> <a
                                                                href="/admin/clients/client/<?= $items->userid; ?>"> <?= $items->phonenumber; ?></a>
                                                    </td>
                                                    <td>
                                                        <a href="/admin/staff/member/<?= $items->staff_id; ?>"><?= $items->firstname . ' ' . $items->lastname; ?>
                                                    </td>
                                                    <td>
                                                        <b><?= $items->item_name . '</b><br/>' . getDescriptionFristLine($items->item_description) . '<br/>' . $items->qty . ' * ' . $items->item_rate; ?>
                                                    </td>
                                                    <td><?= $items->qty * $items->item_rate; ?></td>
                                                    <td><?= date("d-m-Y", strtotime($items->item_start_date)); ?></td>
                                                    <td><?= date("d-m-Y", strtotime($items->item_end_date)); ?></td>
                                                    <td>
                                                        <?php
                                                        $invoice_accepted = get_invoice_status(trim($items->rel_id));
                                                        $invStatus = '<span id="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                                                        if ($invoice_accepted <> null) {

                                                            if ($invoice_accepted->invst_status == 'open') {
                                                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-info">OPEN</span></span>';
                                                            }
                                                            if ($invoice_accepted->invst_status == 'accepted') {
                                                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label-success label">ACCEPTED</span></span>';
                                                            }
                                                            if ($invoice_accepted->invst_status == 'rejected') {
                                                                $invStatus = '<span class="invoiceApprovalStatus-' . trim($items->rel_id) . '"><span class="label label-danger">REJECTED</span></span>';
                                                            }
                                                        }

                                                        echo $invStatus;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <select class="btn btn-primary dropdown-toggle changeInvoiceAcceptsStatus"
                                                                id="<?= $items->rel_id; ?>">

                                                            <option value=""><a href="#" id="open">Modify</a>
                                                            </option>
                                                            <option value="open"><a href="#" id="open">Open</a></option>
                                                            <option value="accepted"><a href="#"
                                                                                        id="accepted">Accepted</a>
                                                            </option>
                                                            <option value="rejected"><a href="#"
                                                                                        id="rejected">Rejected</a>
                                                            </option>
                                                        </select>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($items->custom_note != '') {
                                                            echo '<a href="#" class="renewal_details" id="' . $items->invoice_itmes_id . '">show</a>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="9" style="text-align: right;"></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 small-table-right-col">
                        <div id="proposal" class="hide">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<div id="myModal" class="modal fade col-lg-12" role="dialog" aria-hidden="true"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body" id="content_renewal">
             </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<a href="#" id="modalBox" data-toggle="modal" data-target="#myModal" class="hidden"></a>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script>
    $(document).ready(function () {

        var table = $('#InvoiceItems').DataTable({
            "footerCallback": function (row, data, start, end, display) {

                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    });

                // Total over this page
                pageTotal = api
                    .column(5, {page: 'current'})
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(5).footer()).html(
                    '<strong>Page Total: </strong>' + pageTotal + ' ( <strong>Total: </strong>' + total + ')'
                );
            },
            "pageLength": 30
        });


        $(document).on('click', '.ClientsItemsRenewable', function () {

            // event.preventDefault();

            // alert($(this).is(":checked"));
            var id = $(this).attr("id");

            if ($(this).is(":checked")) {

                $('.items-renewals').find("input[type='checkbox']").prop('disabled', true);
                $('.items-renewals').find('.same-client-' + id).prop('disabled', false);
                $('.client-items-' + id).css('background', 'wh');

            } else {
                if ($('.same-client-' + id + ':checked').length <= 0) {
                    $('.items-renewals').find("input[type='checkbox']").prop('disabled', false);
                    $('.items-renewals').find("input[type='checkbox']").prop('disabled', false);
                }
            }
        });


        $("#create_invoice").click(function () {

            event.preventDefault();
            var data = [];
            var client = '';
            var i = 0;
            var id = $(this).attr('id');


            $('input[name="ItemsRenewable"]:checked').each(function () {

                var val = this.value.split("-");
                client = val[1];
                data[i] = val[0];
                i++;
            });
            var invoiceId = $("#inv-" + client).val();

            window.open(
                '/admin/invoices/invoice?customer_id=' + client + '&renew-items=' + data + '&prev-invoice=' + invoiceId,
                '_blank' // <- This is what makes it open in a new window.
            );
            // window.location.href =

        });


        $(".renewal_details").click(function (e) {
            e.preventDefault();

            var item_id = $(this).attr('id');
            $.ajax({
                url: "/admin/invoices/get_renewal_item_detail",
                type: 'POST',
                data: {"itemId": item_id},
                success: function (data) {
                    console.log(data);
                    if (data != null && data != "") {
                        // tinymce.get('custom_note').setContent(data);
                        $("#content_renewal").html(data);
                        $("#modalBox").trigger('click');
                    }
                },
                error: function (e) {
                    alert("Error!");
                }
            });
        });

    });
</script>
<style>
    th {
        white-space: nowrap;
    }
</style>
</body>
</html>
