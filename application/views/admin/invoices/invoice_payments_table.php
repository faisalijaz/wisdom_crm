<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th><?php echo _l('payments_table_number_heading'); ?></th>
                <th><?php echo _l('payments_table_mode_heading'); ?></th>
                <th><?php echo _l('payments_table_date_heading'); ?></th>
                <th><?php echo _l('invoice_payments_table_discount_heading'); ?></th>
                <th><?php echo _l('payments_table_amount_heading'); ?></th>
                <!-- <th><?php echo _l('options'); ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach($invoice->payments as $payment){ ?>
            <tr class="payment">
                <td><a href="/admin/receipts/details/<?php echo $payment['receipt_id']; ?>" target="_blank"> <?= format_receipt_number(getReceiptNumById($payment['receipt_id'])); ?>  </a>
                    <a href="/admin/receipts/pdf/<?= $payment['receipt_id'];?>" class="pull-right btn btn-default btn-with-tooltip" data-toggle="tooltip" title="" data-placement="bottom" style="margin-right: 5px;" data-original-title="View PDF">
                        <i class="fa fa-file-pdf-o"></i>
                    </a>
                 </td>
                <td><?php echo $payment['name']; ?>
                    <?php if(!empty($payment['paymentmethod'])){
                        echo ' - ' . $payment['paymentmethod'];
                    }
                    if($payment['transactionid']){
                        echo '<br />'._l('payments_table_transaction_id',$payment['transactionid']);
                    }
                    ?>

                </td>
                <td><?php echo _d($payment['date']); ?></td>
                <td><?php echo format_money($payment['discount'],$invoice->symbol.' '); ?></td>
                <td><?php echo format_money($payment['amount'],$invoice->symbol.' '); ?></td>
                <!--
                <td>
                    <a href="<?php echo admin_url('payments/payment/'.$payment['paymentid']); ?>" class="btn btn-default btn-icon"><i class="fa fa-pencil-square-o"></i></a>
                    <?php if(has_permission('payments','','delete')){ ?>
                    <a href="<?php echo admin_url('invoices/delete_payment/'.$payment['paymentid'] . '/' . $payment['invoiceid']); ?>" class="btn btn-danger btn-icon _delete"><i class="fa fa-remove"></i></a>
                    <?php } ?>
                </td>
                -->
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
